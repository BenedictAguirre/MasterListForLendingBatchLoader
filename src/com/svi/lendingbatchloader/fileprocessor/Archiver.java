package com.svi.lendingbatchloader.fileprocessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.svi.lendingbatchloader.config.Config;
import com.svi.lendingbatchloader.constants.Constants;
import com.svi.lendingbatchloader.objects.Batch;

public class Archiver {
	public boolean moveToArchive(String archivePath, Batch batch) {
		File excelFile = new File(batch.getFilePath());
		File archiveFolder = new File(archivePath);
		if (archiveFolder.mkdir()) {
			System.out.println("Archive Folder Created!");
		}
		String fileName = excelFile.getName();
		fileName = renameFilenameIfHasDuplicate(fileName, archiveFolder);
		FileSystem fileSys = FileSystems.getDefault();
		Path dest = fileSys.getPath(archivePath + Constants.FILE_DELIMITER.getString() + fileName);
		Path src = fileSys.getPath(batch.getFilePath());
		try {
			Files.move(src, dest);
			System.out.println("File: " + src.toString() + " is moved to: " + dest.toString());
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			String errorString = "Cannot move file to archive";
			batch.getErrors().add(errorString);
			return false;
		}
	}
	private String renameFilenameIfHasDuplicate(String fileName, File xmlDirectory) {
		List<Path> result = new ArrayList<>();
		String rawFileName = fileName.substring(0, fileName.indexOf('.'));
		String fileExtension = fileName.substring(fileName.indexOf('.'), fileName.length());
		try (Stream<Path> walk = Files.walk(Paths.get(xmlDirectory.getAbsolutePath()))) {
			result = walk.filter(Files::isRegularFile).collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT.getString());
		Date date = new Date(System.currentTimeMillis());
		// System.out.println(formatter.format(date));
		List<File> duplicateFiles = new ArrayList<>();
		boolean dontExist = true;
		for (Path path : result) {
			File i = new File(path.toString());
			if (i.getName().contains(fileName.substring(0, fileName.indexOf('.')))) {
				duplicateFiles.add(i);
				dontExist = false;
			}
		}
		if (!dontExist) {
			fileName = rawFileName + "_" + formatter.format(date) + fileExtension;
		} else {
			fileName = rawFileName + fileExtension;
		}
		return fileName;

	}
}
