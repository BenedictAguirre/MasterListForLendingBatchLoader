package com.svi.lendingbatchloader.fileprocessor;

import java.io.File;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.svi.lendingbatchloader.config.Config;
import com.svi.lendingbatchloader.constants.Constants;

public class Logger {
	private static final String LOG_FILE_PATH = Config.getProperty("LogFilePath");
	private String getCurrentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT.getString());
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}
	public String createLogFile() {
		String fileName = Constants.LOG_FILE_NAME.getString();
		String fileDelimiter = Constants.FILE_DELIMITER.getString();
		File logFolder = new File(LOG_FILE_PATH);
		if (!logFolder.exists()) {
			logFolder.mkdir();
		}
		File logFile = new File(LOG_FILE_PATH+fileDelimiter+fileName+getCurrentDate()+Constants.TEXT_FILE_EXTENSION.getString());
		try {
			if (logFile.createNewFile()) {
				System.out.println("logfile Created");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return logFile.getAbsolutePath();
	}
	
	public void writeToLogFile(String logFilePath, String message) {
		try {
		    Files.write(Paths.get(logFilePath), message.getBytes(), StandardOpenOption.APPEND);
		}catch (IOException e) {
		    //exception handling left as an exercise for the reader
		}
	}

}
