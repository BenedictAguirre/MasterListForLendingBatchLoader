package com.svi.lendingbatchloader.fileprocessor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.svi.lendingbatchloader.config.Config;
import com.svi.lendingbatchloader.constants.Constants;
import com.svi.lendingbatchloader.excel.ExcelReader;
import com.svi.lendingbatchloader.objects.Batch;
import com.svi.lendingbatchloader.objects.Fields;
import com.svi.lendingbatchloader.solr.SolrSaver;

public class FileProcessor {
	private static final String INPUT_PATH = Config.getProperty("ExcelPath");
	private static final String URL_STRING = Config.getProperty("SolrUrl");
	private static final String ARCHIVE_PATH = Config.getProperty("ExcelArchivedPath");

	public List<Batch> getBatchFiles(String inputPath) {
		List<String> allFiles = new ArrayList<>();
		List<Batch> batches = new ArrayList<>();
		try (Stream<Path> walk = Files.walk(Paths.get(inputPath))) {
			allFiles = walk.filter(Files::isRegularFile).map(x -> x.toString()).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (String string : allFiles) {
			if (string.endsWith(Constants.EXCEL_EXTENSION.getString())) {
				Batch batch = new Batch(string, getListOfFieldsPerFile(string));
				batches.add(batch);
			}
		}
		return batches;
	}

	public List<Fields> getListOfFieldsPerFile(String inputExcelPath) {
		ExcelReader excelReader = new ExcelReader();
		List<Fields> fieldsPerFile = excelReader.getListOfFields(inputExcelPath);
		return fieldsPerFile;
	}

	public void processFiles() {
		SolrSaver solrSaver = new SolrSaver();
		Archiver archiver = new Archiver();
		Logger logger = new Logger();
		String logFile = logger.createLogFile();
		List<Batch> batches = getBatchFiles(INPUT_PATH);
		for (Batch batch : batches) {
			
			List<String> errors = new ArrayList<String>();
			batch.setErrors(errors);
			if (archiver.moveToArchive(ARCHIVE_PATH, batch) && solrSaver.saveDocumentFieldsToSolr(URL_STRING, batch)) {
				batch.setSuccessful(true);
			} else {
				batch.setSuccessful(false);
			}
			String message;
			if (batch.isSuccessful()) {
				String success = "Success";
				message = batch.getFields().get(0).getLCcode() + ", " + batch.getFields().get(0).getBatch() + ", "
						+ Constants.SUCCESS.getString() + ", " + batch.getErrors().toString().substring(1, batch.getErrors().toString().length()-1)+"\n";
			} else {
				message = batch.getFields().get(0).getLCcode() + ", " + batch.getFields().get(0).getBatch() + ", " + Constants.FAIL.getString()
						+ ", " + batch.getErrors().toString().substring(1, batch.getErrors().toString().length()-1)+"\n";
			}
			logger.writeToLogFile(logFile, message);
			try {
				Thread.sleep(Integer.parseInt(Config.getProperty("CheckingInterval")) * 1000);
			} catch (NumberFormatException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		FileProcessor processor = new FileProcessor();
		processor.processFiles();
	}

}
