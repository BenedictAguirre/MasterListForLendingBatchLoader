package com.svi.lendingbatchloader.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.svi.lendingbatchloader.config.Config;
import com.svi.lendingbatchloader.constants.Constants;
import com.svi.lendingbatchloader.objects.Fields;

public class ExcelReader {
	private static final String FIELD1_CONFIG = Config.getProperty("Field1");
	private static final String FIELD2_CONFIG = Config.getProperty("Field2");
	private static final String FIELD3_CONFIG = Config.getProperty("Field3");

	public List<Fields> getListOfFields(String inputExcelPath) {
		List<Fields> listOfFields = new ArrayList<>();
		XSSFWorkbook wBook;
		try {
			wBook = new XSSFWorkbook(new FileInputStream(inputExcelPath));
			int seqNo = 0;
			XSSFSheet sheet = wBook.getSheetAt(0);
			for (Row row : sheet) {
				String LCcode = getLCcode(inputExcelPath);
				String batch = getBatch(inputExcelPath);
				String field1 = getField(row, FIELD1_CONFIG);
				String field2 = getField(row, FIELD2_CONFIG);
				String field3 = getField(row, FIELD3_CONFIG);
				if (field1 != null && field2 != null && field3 != null) {
					seqNo += 1;
					String sequence = String.valueOf(seqNo);
					listOfFields.add(new Fields(LCcode, batch, field1, field2, field3, sequence));
				}

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listOfFields;
	}

	private String getLCcode(String inputExcelPath) {
		File inputExcelFile = new File(inputExcelPath);
		File parentFolder = new File(inputExcelFile.getParent());
		String parentFolderName = parentFolder.getName();
		return parentFolderName;
	}

	private String getBatch(String inputExcelPath) {
		File inputExcelFile = new File(inputExcelPath);
		String excelFileName = inputExcelFile.getName().substring(0, inputExcelFile.getName().indexOf("."));
		return excelFileName;
	}

	private String getField(Row row, String fieldConfig) {
		String field = null;
		int column = getColumnIndex(fieldConfig.split(Constants.COMMA.getString())[0]);
		int rowStart = Integer.parseInt(fieldConfig.split(Constants.COMMA.getString())[1]) - 1;
		if (row.getRowNum() >= rowStart) {
			field = row.getCell(column).getStringCellValue();
		}
		return field;
	}

	private int getColumnIndex(String index) {
		int result = 0;
		for (int i = 0; i < index.length(); i++) {
			result *= 26;
			result += index.charAt(i) - 'A' + 1;
		}
		return result -= 1;
	}

	public static void main(String[] args) {
		ExcelReader r = new ExcelReader();
		List<Fields> f = r.getListOfFields(
				"C:\\Users\\dimay\\eclipse-workspace\\MasterListForLendingBatchLoader\\resources\\Test\\LC0001\\B0001.xlsx");
		for (Fields fields : f) {
			System.out.println(fields.getLCcode() + "\t|\t" + fields.getBatch() + "\t|\t" + fields.getField1() + "\t|\t"
					+ fields.getField2() + "\t|\t" + fields.getField3() + "\t|\t" + fields.getSequence());
		}
		
	}

}
