package com.svi.lendingbatchloader.objects;

import java.util.List;

public class Batch {
	private String filePath;
	private List<Fields> fields;
	private boolean isSuccessful;
	private List<String> errors;
	public Batch(String filePath, List<Fields> fields) {
		this.filePath = filePath;
		this.fields = fields;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public List<Fields> getFields() {
		return fields;
	}
	public void setFields(List<Fields> fields) {
		this.fields = fields;
	}
	public boolean isSuccessful() {
		return isSuccessful;
	}
	public void setSuccessful(boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	
	
}
