package com.svi.lendingbatchloader.objects;

public class Fields {
	private String LCcode;
	private String batch;
	private String field1;
	private String field2;
	private String field3;
	private String sequence;
	
	public Fields(String LCcode, String batch, String field1, String field2, String field3, String sequence) {
		this.LCcode = LCcode;
		this.batch = batch;
		this.field1 = field1;
		this.field2 = field2;
		this.field3 = field3;
		this.sequence = sequence;
	}
	public String getLCcode() {
		return LCcode;
	}
	public void setLCcode(String lCcode) {
		LCcode = lCcode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	

}
