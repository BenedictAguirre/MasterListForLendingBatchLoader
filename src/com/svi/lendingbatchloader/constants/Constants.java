package com.svi.lendingbatchloader.constants;

public enum Constants {
	EXCEL_EXTENSION(".xlsx"),
	COMMA(","),
	LOG_FILE_NAME("xml generator tool_"),
	DATE_FORMAT("MMddyyyy_hhmmssaa"),
	TEXT_FILE_EXTENSION(".txt"),
	FILE_DELIMITER("/"),
	SUCCESS("Success"),
	FAIL("Fail")
	;
	private String constant;

	public String getString() {
		return this.constant;
	}

	private Constants(String constant) {
		this.constant = constant;
	}

}
