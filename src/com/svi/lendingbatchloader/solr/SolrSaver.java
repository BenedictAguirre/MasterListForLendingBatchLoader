package com.svi.lendingbatchloader.solr;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

import com.svi.lendingbatchloader.objects.Batch;
import com.svi.lendingbatchloader.objects.Fields;

public class SolrSaver {
	public boolean saveDocumentFieldsToSolr(String urlString, Batch batch) {
		boolean isSuccessful = true;
		for (Fields fields : batch.getFields()) {
			if (saveToSolr(urlString, fields)) {
				System.out.println("Added LCcode: " + fields.getLCcode() + ", Batch: " + fields.getBatch()
					+ ", Field1: " + fields.getField1() + ", Field2: " + fields.getField2() + ", Field3: "
					+ fields.getField3() + ", Sequence: " + fields.getSequence()+ " successfully");
			} else {
				String message = "Cannot add LCcode: " + fields.getLCcode() + ", Batch: " + fields.getBatch()
						+ ", Field1: " + fields.getField1() + ", Field2: " + fields.getField2() + ", Field3: "
						+ fields.getField3() + ", Sequence: " + fields.getSequence() + " to Solr";
				batch.getErrors().add(message);
				isSuccessful = false;
			}
		}
		return isSuccessful;
	}

	public boolean saveToSolr(String urlString, Fields fields) {
		SolrClient Solr = new HttpSolrClient.Builder(urlString).build();
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("LCcode", fields.getLCcode());
		doc.addField("batch", fields.getBatch());
		doc.addField("field_1", fields.getField1());
		doc.addField("field_2", fields.getField2());
		doc.addField("field_3", fields.getField3());
		doc.addField("sequence", fields.getSequence());
		boolean isSuccessful = false;
		try {
			Solr.add(doc);
			Solr.commit();
			isSuccessful = true;
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isSuccessful;
	}
}
